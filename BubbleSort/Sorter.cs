﻿using System;

// ReSharper disable InconsistentNaming
#pragma warning disable SA1611

namespace BubbleSort
{
    public static class Sorter
    {
        /// <summary>
        /// Sorts an <paramref name="array"/> with bubble sort algorithm.
        /// </summary>
        public static void BubbleSort(this int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length - 1; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        (array[j], array[j + 1]) = (array[j + 1], array[j]);
                    }
                }
            }
        }

        /// <summary>
        /// Sorts an <paramref name="array"/> with recursive bubble sort algorithm.
        /// </summary>
        public static void RecursiveBubbleSort(this int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            RecursiveBubbleSort(array, array.Length);
        }

        public static void RecursiveBubbleSort(this int[] array, int length)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (length <= 1)
            {
                return;
            }

            RecursiveBubbleSort(array, length - 1);

            RecursiveBubbleSwap(array, 0);
        }

        public static void RecursiveBubbleSwap(this int[] array, int startIndex)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (startIndex >= array.Length - 1)
            {
                return;
            }

            if (array[startIndex] > array[startIndex + 1])
            {
                (array[startIndex], array[startIndex + 1]) = (array[startIndex + 1], array[startIndex]);
            }

            RecursiveBubbleSwap(array, startIndex + 1);
        }
    }
}
